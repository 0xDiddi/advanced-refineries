package diddi.advancedrefineries

object Constants {

    object Block {
        final val blockConveyor = "block_conveyor"
        final val blockCrusherController = "block_crusher_controller"
        final val blockHeavyStructure = "block_heavy_structure"
        final val blockLightStructure = "block_light_structure"
        final val blockActuator = "block_actuator"
        final val blockPowerLine = "block_power_line"
    }

    object TileEntity {
        final val tileEntityConveyor = "tileEntityConveyor"
    }

    object Container {}

}
