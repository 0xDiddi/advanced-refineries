package diddi.advancedrefineries.multiblock

import diddi.advancedrefineries.block.BlockManager
import diddi.advancedrefineries.multiblock.base.MappedMultiBlockStructure
import net.minecraft.block.Block
import net.minecraft.init.Blocks
import net.minecraft.util.math.BlockPos

class MultiBlockCrusher extends MappedMultiBlockStructure {
    override val blockMapping: Map[String, Block] = Map(
        "l" -> BlockManager.blockLightStructure,
        "h" -> BlockManager.blockHeavyStructure,
        "a" -> BlockManager.blockActuator,
        " " -> Blocks.AIR
    )

    override val structure: List[String] = List(
        "l", "l", "l",
        "l", " ", "l",
        "l", "l", "l",

        "h", "a", "h",
        "a", " ", "a",
        "h", "a", "h",

        " ", "l", " ",
        "l", " ", "l",
        " ", "l", " "
    )

    override val width: Int = 3
    override val depth: Int = 3
    override val startPos: List[BlockPos] = List(new BlockPos(1, 1, 1))
}
