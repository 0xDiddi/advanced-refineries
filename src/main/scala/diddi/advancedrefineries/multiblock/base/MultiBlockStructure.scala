package diddi.advancedrefineries.multiblock.base

import net.minecraft.block.Block
import net.minecraft.init.Blocks
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

abstract class MultiBlockStructure {

    final type Matrix = Array[Array[Array[Block]]]

    val startPos: List[BlockPos]

    /**
      * Checks the world for a match
      * @param world The world in which to check for a match
      * @param startPos The position at which to check
      * @param ignoreEmpty Whether or not air blocks in the matrix are skipped when checking
      * @param lambda What to do with a block on match
      * @return Whether the matrix was matched or not
      */
    def checkMatrix(world: World, startPos: BlockPos, ignoreEmpty: Boolean, lambda: (World, BlockPos) => Unit): Boolean = {
        val matrix = this.deriveActualMatrix()
        var offset_x: Int = 0
        var offset_y: Int = 0
        var offset_z: Int = 0

        var flag = true

        def check(): Unit = {
            for (y <- matrix.indices) {
                for (x <- matrix(y).indices) {
                    for (z <- matrix(y)(x).indices) {
                        val worldPos = new BlockPos(x, y, z).add(offset_x, offset_y, offset_z)
                        val block = world.getBlockState(worldPos).getBlock
                        if (block != matrix(y)(x)(z)) {
                            if (!(ignoreEmpty && matrix(y)(x)(z) == Blocks.AIR)) {
                                flag = false
                                return
                            }
                        }
                    }
                }
            }
        }

        def remove(): Unit = {
            for (y <- matrix.indices) {
                for (x <- matrix(y).indices) {
                    for (z <- matrix(y)(x).indices) {
                        val worldPos = new BlockPos(x, y, z).add(offset_x, offset_y, offset_z)
                        if (matrix(y)(x)(z) != Blocks.AIR)
                            lambda(world, worldPos)
                    }
                }
            }
        }

        for (pos: BlockPos <- this.startPos) {
            offset_x = startPos.getX - pos.getX
            offset_y = startPos.getY - pos.getY
            offset_z = startPos.getZ - pos.getZ
            flag = true

            check()

            if (flag) {
                if (lambda != null)
                    remove()

                return true
            }
        }

        false
    }

    def deriveActualMatrix(): Matrix

}
