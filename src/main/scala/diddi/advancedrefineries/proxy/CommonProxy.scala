package diddi.advancedrefineries.proxy

import net.minecraft.item.ItemStack
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}

class CommonProxy {

    def preInit(event: FMLPreInitializationEvent): Unit = {

    }

    def init(event: FMLInitializationEvent): Unit = {

    }

    def postInit(event: FMLPostInitializationEvent): Unit = {

    }

    def addItemRenderer(stack: ItemStack, resLoc: ResourceLocation): Unit = {

    }

}
