package diddi.advancedrefineries.item

import diddi.advancedrefineries.AdvancedRefineries
import diddi.advancedrefineries.gui.GuiHandler
import diddi.advancedrefineries.item.base.ItemARBase
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.{Item, ItemStack}
import net.minecraft.util.ResourceLocation
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent

import scala.collection.mutable.ListBuffer

@Mod.EventBusSubscriber
object ItemManager {
    var registeredItems: ListBuffer[ItemARBase] = ListBuffer()

    val itemMachinistHandbook: ItemUiOpener = new ItemUiOpener("item_machinists_handbook", GuiHandler.HANDBOOK_ID)
    val itemBeginnersGuide: ItemUiOpener = new ItemUiOpener("item_beginners_guide", GuiHandler.HANDBOOK_ID)

    def init(): Unit = {
        registeredItems += itemMachinistHandbook
        registeredItems += itemBeginnersGuide

        MinecraftForge.EVENT_BUS.register(this.getClass)
    }

    @SubscribeEvent
    def registerItems(evt: RegistryEvent.Register[Item]): Unit = {
        evt.getRegistry.registerAll(registeredItems: _*)

        for (item: ItemARBase <- registeredItems) {
            registerRendering(item)
            item.setCreativeTab(CreativeTabs.TOOLS)
        }
    }

    def registerRendering(item: ItemARBase): Unit = {
        AdvancedRefineries.proxy.addItemRenderer(
            new ItemStack(item),
            new ResourceLocation(AdvancedRefineries.MOD_ID, item.getName))
    }

}
