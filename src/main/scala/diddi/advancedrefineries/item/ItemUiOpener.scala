package diddi.advancedrefineries.item

import diddi.advancedrefineries.AdvancedRefineries
import diddi.advancedrefineries.item.base.ItemARBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.util.{ActionResult, EnumHand}
import net.minecraft.world.World

class ItemUiOpener(name: String, guiId: Int) extends ItemARBase(name) {
    override def onItemRightClick(worldIn: World, playerIn: EntityPlayer, handIn: EnumHand): ActionResult[ItemStack] = {
        playerIn.openGui(AdvancedRefineries, guiId, worldIn, 0, 0, 0)
        super.onItemRightClick(worldIn, playerIn, handIn)
    }
}
