package diddi.advancedrefineries.item.base

import net.minecraft.item.Item
import diddi.advancedrefineries.AdvancedRefineries

class ItemARBase(final val name: String) extends Item {

    this.setUnlocalizedName(AdvancedRefineries.MOD_ID + "." + name)
    this.setRegistryName(AdvancedRefineries.MOD_ID, name)

    def getName: String = name

}
