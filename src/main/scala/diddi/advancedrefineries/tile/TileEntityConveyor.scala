package diddi.advancedrefineries.tile

import net.minecraft.block.BlockContainer
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.{Entity, MoverType}
import net.minecraft.init.Blocks
import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.EnumFacing
import net.minecraft.util.math.AxisAlignedBB
import diddi.advancedrefineries.block.BlockConveyor
import diddi.advancedrefineries.config.values.ConfigIntValues
import diddi.advancedrefineries.tile.base.TileEntityARBase

import scala.collection.JavaConversions._

class TileEntityConveyor extends TileEntityARBase {

    var tmpCompound: NBTTagCompound = _
    var shouldRestoreState: Boolean = _

    //noinspection SpellCheckingInspection
    var aabb: Map[EnumFacing, AxisAlignedBB] = _

    var currentSlotForExtraction = 0
    var extractionTimer = 0

    override def update(): Unit = {
        if (shouldRestoreState && this.world != null) {
            var state = this.world.getBlockState(this.pos)
            state = state.withProperty(BlockConveyor.HEADING, EnumFacing.getFront(tmpCompound.getInteger("heading")))
            state = state.withProperty(BlockConveyor.HAS_PUSH, tmpCompound.getBoolean("push").asInstanceOf[java.lang.Boolean])
            state = state.withProperty(BlockConveyor.HAS_PULL, tmpCompound.getBoolean("pull").asInstanceOf[java.lang.Boolean])
            tmpCompound = null
            this.world.setBlockState(this.pos, state)
            shouldRestoreState = false
        }
        if (aabb == null)
            aabb = Map(
                (EnumFacing.EAST, new AxisAlignedBB(this.pos.add(1, 0, 0), this.pos.add(1, 1, 1))),
                (EnumFacing.WEST, new AxisAlignedBB(this.pos.add(0, 0, 0), this.pos.add(0, 1, 1))),
                (EnumFacing.SOUTH, new AxisAlignedBB(this.pos.add(0, 0, 1), this.pos.add(1, 1, 1))),
                (EnumFacing.NORTH, new AxisAlignedBB(this.pos.add(0, 0, 0), this.pos.add(1, 1, 0))))

        val state = this.world.getBlockState(this.pos)

        val entities = this.world.getEntitiesWithinAABB(classOf[Entity], new AxisAlignedBB(this.pos.add(0, (1 / 16) * 9, 0), this.pos.add(1, 1, 1)))
        val dir = state.getValue(BlockConveyor.HEADING)

        for (e: Entity <- entities.toList) {
            e.move(MoverType.SELF, dir.getFrontOffsetX / 12.0, 0, dir.getFrontOffsetZ / 12.0)

            //noinspection TypeCheckCanBeMatch
            if (e.isInstanceOf[EntityItem])
                e.asInstanceOf[EntityItem].setNoDespawn()

            if (dir == EnumFacing.NORTH || dir == EnumFacing.SOUTH) {
                e.move(MoverType.SELF, ((e.posX - (this.pos.getX + 0.5)) * -1) / 10, 0, 0)
            } else if (dir == EnumFacing.EAST || dir == EnumFacing.WEST) {
                e.move(MoverType.SELF, 0, 0, ((e.posZ - (this.pos.getZ + 0.5)) * -1) / 10)
            }
        }

        val front_state = this.world.getBlockState(this.pos.offset(dir))
        if (state.getValue(BlockConveyor.HAS_PUSH) && front_state.getBlock.isInstanceOf[BlockContainer] && front_state.getBlock.hasTileEntity(front_state)) {
            val entities_for_storage = this.world.getEntitiesWithinAABB(classOf[EntityItem], aabb(dir))

            val tile = world.getTileEntity(this.pos.offset(dir))
            for (e: EntityItem <- entities_for_storage.toList)
                if (tryInsertIntoTile(e.getItem, tile)) e.setDead()
        } else {
            val dropped_entities = this.world.getEntitiesWithinAABB(classOf[EntityItem], new AxisAlignedBB(this.pos.offset(dir), this.pos.add(1, 1, 1).offset(dir)))

            for (e: EntityItem <- dropped_entities.toList)
                e.setAgeToCreativeDespawnTime()
        }

        val back_state = this.world.getBlockState(this.pos.offset(dir.getOpposite))
        if (state.getValue(BlockConveyor.HAS_PULL) && back_state.getBlock.isInstanceOf[BlockContainer] && back_state.getBlock.hasTileEntity(back_state) && extractionTimer == 0) {
            val tile = this.world.getTileEntity(this.pos.offset(dir.getOpposite))
            //noinspection TypeCheckCanBeMatch
            if (tile.isInstanceOf[IInventory]) {
                var done = false

                do {
                    val inv = tile.asInstanceOf[IInventory]
                    val slot = inv.getStackInSlot(currentSlotForExtraction)
                    if (slot.getItem != new ItemStack(Blocks.AIR).getItem) {
                        val entity = new EntityItem(this.world, this.pos.getX + 0.5, this.pos.getY + 1.5, this.pos.getZ + 0.5, slot)
                        entity.motionX = 0
                        entity.motionY = 0
                        entity.motionZ = 0
                        this.world.spawnEntity(entity)
                        inv.removeStackFromSlot(currentSlotForExtraction)
                        done = true
                    }
                    currentSlotForExtraction += 1
                    if (currentSlotForExtraction >= inv.getSizeInventory) {
                        currentSlotForExtraction = 0
                        done = true
                    }
                } while (!done)
            }
        }
        extractionTimer += 1
        if (extractionTimer >= ConfigIntValues.CONVEYOR_EXTRACTION_TIME.currentValue)
            extractionTimer = 0
    }

    private def tryInsertIntoTile(stack: ItemStack, tile: TileEntity): Boolean = {
        //noinspection TypeCheckCanBeMatch
        if (tile.isInstanceOf[IInventory]) {
            val ii = tile.asInstanceOf[IInventory]
            val slots = ii.getSizeInventory
            do {
                for (i <- 0 until slots) {
                    val slot = ii.getStackInSlot(i)
                    if (slot.getItem == stack.getItem && slot.getCount < slot.getMaxStackSize) {
                        val newsize = Math.min(slot.getCount + stack.getCount, slot.getMaxStackSize)
                        val diff = newsize - slot.getCount
                        slot.setCount(newsize)
                        stack.shrink(diff)
                        if (stack.getCount == 0)
                            return true
                    } else if (slot.getItem == new ItemStack(Blocks.AIR).getItem) {
                        ii.setInventorySlotContents(i, stack)
                        return true
                    } else if (i == slots - 1)
                        return false
                }
            } while (stack != null && stack.getCount > 0)
        }
        false
    }

    override def writeToNBT(compound: NBTTagCompound): NBTTagCompound = {
        val state = this.world.getBlockState(this.pos)
        compound.setInteger("heading", state.getValue(BlockConveyor.HEADING).getIndex)
        compound.setBoolean("push", state.getValue(BlockConveyor.HAS_PUSH))
        compound.setBoolean("pull", state.getValue(BlockConveyor.HAS_PULL))
        super.writeToNBT(compound)
    }

    override def readFromNBT(compound: NBTTagCompound): Unit = {
        tmpCompound = compound
        shouldRestoreState = true
        super.readFromNBT(compound)
    }
}
