package diddi.advancedrefineries.tile.base

import net.minecraft.entity.player.EntityPlayer
import net.minecraft.init.Blocks
import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack
import net.minecraft.nbt.{NBTTagCompound, NBTTagList}

abstract class TileEntityARInventoryBase(final val name: String, final val slotCount: Int) extends TileEntityARBase with IInventory {

    var slots: Array[ItemStack] = new Array[ItemStack](slotCount)

    private var isDirty: Boolean = false

    override def update(): Unit = {
        if (this.isDirty) {
            this.sendUpdate()
            this.isDirty = false
        }
    }

    override def decrStackSize(index: Int, count: Int): ItemStack = {
        if (slots(index) != null) {
            var stackAt: ItemStack = null
            if (slots(index).getCount <= count) {
                stackAt = slots(index)
                slots(index) = null
                this.markDirty()
                return stackAt
            }
            else {
                stackAt = slots(index).splitStack(count)
                if (slots(index).getCount == 0) slots(index) = null
                this.markDirty()
                return stackAt
            }
        }
        null
    }

    override def markDirty(): Unit = {
        super.markDirty()
        this.isDirty = true
    }

    override def openInventory(player: EntityPlayer): Unit = {}

    override def closeInventory(player: EntityPlayer): Unit = {}

    override def getInventoryStackLimit: Int = 64

    override def clear(): Unit = {
        for (index: Int <- this.slots.indices)
            this.removeStackFromSlot(index)
    }

    override def removeStackFromSlot(index: Int): ItemStack = {
        val stack = this.slots.apply(index)
        this.slots(index) = null
        stack
    }

    override def isItemValidForSlot(index: Int, stack: ItemStack): Boolean = false

    override def setInventorySlotContents(index: Int, stack: ItemStack): Unit = {
        if (index < this.getSizeInventory) {
            this.slots(index) = stack
            this.markDirty()
        }
    }

    // TODO: find new mapping for isUsableByPlayer
    //override def isUseableByPlayer(player: EntityPlayer): Boolean = player.getDistanceSq(this.pos.getX + 0.5D, this.pos.getY + 0.5D, this.pos.getZ + 0.5D) <= 64

    override def getStackInSlot(index: Int): ItemStack = {
        if (index < this.getSizeInventory)
            return this.slots(index)
        null
    }

    override def getSizeInventory: Int = slots.length

    override def readFromNBT(compound: NBTTagCompound) {
        super.readFromNBT(compound)
        if (this.slots.length > 0) {
            val tagList: NBTTagList = compound.getTagList("Items", 10)
            for (currentIndex <- 0 until tagList.tagCount) {
                val tagCompound: NBTTagCompound = tagList.getCompoundTagAt(currentIndex)
                if (currentIndex >= 0 && currentIndex < slots.length) {
                    slots(currentIndex) = new ItemStack(tagCompound)
                }
            }
        }
    }

    override def writeToNBT(compound: NBTTagCompound): NBTTagCompound = {
        super.writeToNBT(compound)
        if (this.slots.length > 0) {
            val tagList: NBTTagList = new NBTTagList
            for (currentIndex <- this.slots.indices) {
                val tagCompound: NBTTagCompound = new NBTTagCompound
                if (this.slots(currentIndex) != null) {
                    this.slots(currentIndex).writeToNBT(tagCompound)
                }
                tagList.appendTag(tagCompound)
            }
            compound.setTag("Items", tagList)
        }
        compound
    }

    override def getFieldCount: Int = 0

    override def setField(id: Int, value: Int): Unit = {}

    override def getField(id: Int): Int = 0

    override def getName: String = name

    override def hasCustomName: Boolean = true
}
