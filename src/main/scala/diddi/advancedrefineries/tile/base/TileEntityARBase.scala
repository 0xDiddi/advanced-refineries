package diddi.advancedrefineries.tile.base

import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.NetworkManager
import net.minecraft.network.play.server.SPacketUpdateTileEntity
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.ITickable
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint
import diddi.advancedrefineries.network.DataHandler
import diddi.advancedrefineries.network.data.NBTPacket

abstract class TileEntityARBase extends TileEntity with ITickable {

    override def update()

    override def getUpdatePacket: SPacketUpdateTileEntity = {
        val tag = this.getUpdateTag
        if (tag != null)
            return new SPacketUpdateTileEntity(this.pos, 0, tag)
        null
    }

    override def onDataPacket(net: NetworkManager, pkt: SPacketUpdateTileEntity): Unit = {
        this.readFromNBT(pkt.getNbtCompound)
    }

    def sendUpdate(): Unit = {
        if (!this.world.isRemote) {
            val tag = this.getUpdateTag
            if (tag != null) {
                val data = new NBTTagCompound
                data.setTag("data", tag)
                data.setInteger("X", this.pos.getX)
                data.setInteger("Y", this.pos.getY)
                data.setInteger("Z", this.pos.getZ)
                DataHandler.network.sendToAllAround(new NBTPacket(data, DataHandler.TILE_ENTITY_HANDLER), new TargetPoint(this.world.provider.getDimension, this.getPos.getX, this.getPos.getY, this.getPos.getZ, 128))
            }
        }
    }

    override def getUpdateTag: NBTTagCompound = {
        val tag = super.getUpdateTag
        this.writeToNBT(tag)
        tag
    }

}
