package diddi.advancedrefineries.tile

import net.minecraftforge.fml.common.registry.GameRegistry
import diddi.advancedrefineries.{AdvancedRefineries, Constants}
import net.minecraft.util.ResourceLocation

object TileEntityManager {

    def init(): Unit = {
        GameRegistry.registerTileEntity(classOf[TileEntityConveyor],
            new ResourceLocation(AdvancedRefineries.MOD_ID, Constants.TileEntity.tileEntityConveyor))
    }

}
