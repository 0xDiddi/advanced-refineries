package diddi.advancedrefineries.network.data

import io.netty.buffer.ByteBuf
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.PacketBuffer
import net.minecraftforge.fml.common.network.simpleimpl.IMessage
import diddi.advancedrefineries.network.DataHandler

class NBTPacket(var data: NBTTagCompound, var handler: DataHandler) extends IMessage {

    def this() = this(null, null)

    override def toBytes(buf: ByteBuf): Unit = {
        val buffer = new PacketBuffer(buf)
        buffer.writeCompoundTag(this.data)
        buffer.writeInt(DataHandler.DATA_HANDLERS.indexOf(this.handler))
    }

    override def fromBytes(buf: ByteBuf): Unit = {
        val buffer = new PacketBuffer(buf)
        this.data = buffer.readCompoundTag()
        val handlerID = buffer.readInt()
        if (handlerID >= 0 && handlerID < DataHandler.DATA_HANDLERS.length) {
            this.handler = DataHandler.DATA_HANDLERS(handlerID)
        }
    }

}
