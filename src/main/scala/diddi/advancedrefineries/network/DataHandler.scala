package diddi.advancedrefineries.network

import net.minecraft.client.Minecraft
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.math.BlockPos
import net.minecraftforge.fml.common.network.NetworkRegistry
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper
import net.minecraftforge.fml.relauncher.Side
import diddi.advancedrefineries.AdvancedRefineries
import diddi.advancedrefineries.network.data.{NBTHandler, NBTPacket}
import diddi.advancedrefineries.tile.base.TileEntityARBase

object DataHandler {

    val TILE_ENTITY_HANDLER: DataHandler = new DataHandler {
        override def handleData(data: NBTTagCompound): Unit = {
            val world = Minecraft.getMinecraft.world
            if (world != null) {
                val tile = world.getTileEntity(new BlockPos(data.getInteger("X"), data.getInteger("Y"), data.getInteger("Z")))
                if (tile != null && tile.isInstanceOf[TileEntityARBase])
                    tile.asInstanceOf[TileEntityARBase].readFromNBT(data.getCompoundTag("data"))
            }
        }
    }
    val DATA_HANDLERS: List[DataHandler] = List[DataHandler](TILE_ENTITY_HANDLER)

    var network: SimpleNetworkWrapper = NetworkRegistry.INSTANCE.newSimpleChannel(AdvancedRefineries.MOD_ID)
    network.registerMessage(classOf[NBTHandler.ClientHandler], classOf[NBTPacket], 0, Side.CLIENT)
    network.registerMessage(classOf[NBTHandler.ServerHandler], classOf[NBTPacket], 1, Side.SERVER)

}

abstract class DataHandler {

    def handleData(data: NBTTagCompound)

}
