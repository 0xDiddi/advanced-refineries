package diddi.advancedrefineries.config.values;

import diddi.advancedrefineries.config.ConfigCategories;

public enum ConfigIntValues {

    CONVEYOR_EXTRACTION_TIME("conveyor extraction time", ConfigCategories.MACHINE_VALUES, 40, "How long the conveyor should wait (in ticks) between extracting two stacks");

    public final String name;
    public final String category;
    public final int defaultValue;
    public final String desc;

    public int currentValue;

    ConfigIntValues(String name, ConfigCategories category, int defaultValue, String desc) {
        this.name = name;
        this.category = category.name;
        this.defaultValue = defaultValue;
        this.desc = desc;
    }

}
