package diddi.advancedrefineries.config.values;

import diddi.advancedrefineries.config.ConfigCategories;

public enum ConfigStringListValues {

    MATERIALS("materials", ConfigCategories.MATERIALS, new String[]{"Iron", "Gold", "Copper", "Tin", "Bronze", "Lead"}, "The materials to do stuff with.");

    public final String name;
    public final String category;
    public final String[] defaultValues;
    public final String desc;

    public String[] currentValues;

    ConfigStringListValues(String name, ConfigCategories category, String[] defaultValues, String desc) {
        this.name = name;
        this.category = category.name;
        this.defaultValues = defaultValues;
        this.desc = desc;
    }

}
