package diddi.advancedrefineries.config;

public enum ConfigCategories {

    MATERIALS("material values", ""),
    MACHINE_VALUES("machine values", "");

    public final String name;
    public final String comment;

    ConfigCategories(String name, String comment) {
        this.name = name;
        this.comment = comment;
    }
}
