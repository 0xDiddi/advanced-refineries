package diddi.advancedrefineries.block

import diddi.advancedrefineries.Constants
import diddi.advancedrefineries.block.base.BlockARBase
import diddi.advancedrefineries.multiblock.MultiBlockCrusher
import net.minecraft.block.material.{MapColor, Material}
import net.minecraft.block.properties.PropertyBool
import net.minecraft.block.state.{BlockStateContainer, IBlockState}
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.util.math.BlockPos
import net.minecraft.util.text.TextComponentString
import net.minecraft.util.{EnumFacing, EnumHand}
import net.minecraft.world.World

object BlockCrusherController {

    final val ACTIVATED: PropertyBool = PropertyBool.create("activated")

}

class BlockCrusherController extends BlockARBase(Material.ANVIL, MapColor.IRON, Constants.Block.blockCrusherController) {

    override def onBlockPlacedBy(worldIn: World, pos: BlockPos, state: IBlockState, placer: EntityLivingBase, stack: ItemStack): Unit = {
        val state = this.getDefaultState.withProperty(BlockCrusherController.ACTIVATED, false.asInstanceOf[java.lang.Boolean])
        worldIn.setBlockState(pos, state)
    }

    override def onBlockActivated(worldIn: World, pos: BlockPos, state: IBlockState, playerIn: EntityPlayer, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean = {
        if (hand != EnumHand.MAIN_HAND) return false
        if (worldIn.isRemote) return false

        val mb = new MultiBlockCrusher
        val b = mb.checkMatrix(worldIn, pos, ignoreEmpty = true, (world, blockPos) => {
            val state = world.getBlockState(blockPos)
            if (state.getBlock.isInstanceOf[BlockHideable]) {
                world.setBlockState(blockPos, state.withProperty(BlockHideable.HIDDEN, true.asInstanceOf[java.lang.Boolean]))
            }
        })

        playerIn.sendMessage(new TextComponentString("match: " + b))

        if (b)
            worldIn.setBlockState(pos, this.getDefaultState.withProperty(BlockCrusherController.ACTIVATED, true.asInstanceOf[java.lang.Boolean]))

        b
    }

    override def breakBlock(worldIn: World, pos: BlockPos, state: IBlockState): Unit = {
        super.breakBlock(worldIn, pos, state)

        val mb = new MultiBlockCrusher
        val b = mb.checkMatrix(worldIn, pos, ignoreEmpty = true, (world, blockPos) => {
            val state = world.getBlockState(blockPos)
            if (state.getBlock.isInstanceOf[BlockHideable]) {
                world.setBlockState(blockPos, state.withProperty(BlockHideable.HIDDEN, false.asInstanceOf[java.lang.Boolean]))
            }
        })
    }

    override def getMetaFromState(state: IBlockState): Int = 0

    override def createBlockState(): BlockStateContainer = new BlockStateContainer(this, BlockCrusherController.ACTIVATED)
}
