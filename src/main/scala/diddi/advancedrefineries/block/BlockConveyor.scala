package diddi.advancedrefineries.block

import net.minecraft.block.{Block, ITileEntityProvider}
import net.minecraft.block.material.{MapColor, Material}
import net.minecraft.block.properties.{PropertyBool, PropertyDirection}
import net.minecraft.block.state.{BlockStateContainer, IBlockState}
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.init.Blocks
import net.minecraft.item.{Item, ItemStack}
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.math.{AxisAlignedBB, BlockPos}
import net.minecraft.util.{EnumFacing, EnumHand}
import net.minecraft.world.{IBlockAccess, World}
import diddi.advancedrefineries.Constants
import diddi.advancedrefineries.block.base.BlockARBase
import diddi.advancedrefineries.tile.TileEntityConveyor

object BlockConveyor {

    /**
      * Where this conveyor is facing. (in which direction items are transported)
      */
    final val HEADING: PropertyDirection = PropertyDirection.create("heading")
    final val HAS_MODULE: PropertyBool = PropertyBool.create("has_module")
    final val HAS_PULL: PropertyBool = PropertyBool.create("has_pull")
    final val HAS_PUSH: PropertyBool = PropertyBool.create("has_push")

    final val BASE_AABB = new AxisAlignedBB(0, 0, 0, 1, 0.6875, 1)
    final val MODULE_AABB = new AxisAlignedBB(0, 0, 0, 1, 1, 1)
    final val COLLISION_AABB = new AxisAlignedBB(0, 0.3125, 0, 1, 0.6875, 1)

}

class BlockConveyor extends BlockARBase(Material.IRON, MapColor.IRON, Constants.Block.blockConveyor) with ITileEntityProvider {

    this.setCreativeTab(CreativeTabs.TRANSPORTATION)

    override def createNewTileEntity(worldIn: World, meta: Int): TileEntity = new TileEntityConveyor

    override def onBlockPlacedBy(worldIn: World, pos: BlockPos, state: IBlockState, placer: EntityLivingBase, stack: ItemStack): Unit = {
        var state = this.getDefaultState
        state = state.withProperty(BlockConveyor.HAS_PUSH, false.asInstanceOf[java.lang.Boolean])
        state = state.withProperty(BlockConveyor.HAS_PULL, false.asInstanceOf[java.lang.Boolean])

        state = state.withProperty(BlockConveyor.HEADING, EnumFacing.fromAngle(placer.rotationYaw))

        state = state.withProperty(BlockConveyor.HAS_MODULE, hasModule(worldIn, pos, state).asInstanceOf[java.lang.Boolean])

        worldIn.setBlockState(pos, state)
    }

    override def onBlockActivated(worldIn: World, pos: BlockPos, state: IBlockState, playerIn: EntityPlayer, hand: EnumHand, side: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean = {
        if (hand != EnumHand.MAIN_HAND)
            return false

        val heldItem = playerIn.getHeldItemMainhand
        if (Block.getBlockFromItem(heldItem.getItem) != Blocks.AIR) {
            // Add push/pull
            if (heldItem.getItem == Item.getItemFromBlock(Blocks.PISTON) && !state.getValue(BlockConveyor.HAS_PUSH)) {
                worldIn.setBlockState(pos, state.withProperty(BlockConveyor.HAS_PUSH, true.asInstanceOf[java.lang.Boolean]))
                heldItem.shrink(1)
                return true
            } else if (heldItem.getItem == Item.getItemFromBlock(Blocks.STICKY_PISTON) && !state.getValue(BlockConveyor.HAS_PULL)) {
                worldIn.setBlockState(pos, state.withProperty(BlockConveyor.HAS_PULL, true.asInstanceOf[java.lang.Boolean]))
                heldItem.shrink(1)
                return true
            }
        } else {
            // remove push/pull
            if (state.getValue(BlockConveyor.HAS_PUSH) && side == state.getValue(BlockConveyor.HEADING)) {
                playerIn.inventory.addItemStackToInventory(new ItemStack(Blocks.PISTON))
                worldIn.setBlockState(pos, state.withProperty(BlockConveyor.HAS_PUSH, false.asInstanceOf[java.lang.Boolean]))
                return true
            } else if (state.getValue(BlockConveyor.HAS_PULL) && side == state.getValue(BlockConveyor.HEADING).getOpposite) {
                playerIn.inventory.addItemStackToInventory(new ItemStack(Blocks.STICKY_PISTON))
                worldIn.setBlockState(pos, state.withProperty(BlockConveyor.HAS_PULL, false.asInstanceOf[java.lang.Boolean]))
                return true
            }
        }

        false
    }

    override def breakBlock(worldIn: World, pos: BlockPos, state: IBlockState): Unit = {
        super.breakBlock(worldIn, pos, state)
        if (state.getValue(BlockConveyor.HAS_PULL)) {
            worldIn.spawnEntity(new EntityItem(worldIn, pos.getX, pos.getY, pos.getZ, new ItemStack(Blocks.STICKY_PISTON)))
        }
        if (state.getValue(BlockConveyor.HAS_PUSH)) {
            worldIn.spawnEntity(new EntityItem(worldIn, pos.getX, pos.getY, pos.getZ, new ItemStack(Blocks.PISTON)))
        }
    }

    override def getCollisionBoundingBox(state: IBlockState, worldIn: IBlockAccess, pos: BlockPos): AxisAlignedBB = {
        BlockConveyor.COLLISION_AABB
    }

    override def getBoundingBox(state: IBlockState, source: IBlockAccess, pos: BlockPos): AxisAlignedBB = {
        if (this.getActualState(state, source, pos).getValue(BlockConveyor.HAS_MODULE))
            return BlockConveyor.MODULE_AABB
        BlockConveyor.BASE_AABB
    }

    override def getActualState(state: IBlockState, worldIn: IBlockAccess, pos: BlockPos): IBlockState = {
        state.withProperty(BlockConveyor.HAS_MODULE, hasModule(worldIn, pos, state).asInstanceOf[java.lang.Boolean])
    }

    def hasModule(world: IBlockAccess, pos: BlockPos, state: IBlockState): Boolean = {
        val block = world.getBlockState(pos.up)
        block.getBlock.isInstanceOf[BlockConveyor] && (block.getValue(BlockConveyor.HEADING) == state.getValue(BlockConveyor.HEADING) || block.getValue(BlockConveyor.HEADING) == state.getValue(BlockConveyor.HEADING).getOpposite)
    }

    override def isOpaqueCube(state: IBlockState): Boolean = false

    override def isFullBlock(state: IBlockState): Boolean = false

    override def getMetaFromState(state: IBlockState): Int = 0

    override def isPassable(worldIn: IBlockAccess, pos: BlockPos): Boolean = false

    // todo: IBlockState.getBlockFaceShape is the replacement here, but where to put that?
    // override def isBlockSolid(worldIn: IBlockAccess, pos: BlockPos, side: EnumFacing): Boolean = false

    override def canPlaceTorchOnTop(state: IBlockState, world: IBlockAccess, pos: BlockPos): Boolean = false

    override def createBlockState(): BlockStateContainer = new BlockStateContainer(this, BlockConveyor.HEADING, BlockConveyor.HAS_MODULE, BlockConveyor.HAS_PULL, BlockConveyor.HAS_PUSH)

}
