package diddi.advancedrefineries.block.base

import net.minecraft.block.Block
import net.minecraft.block.material.{MapColor, Material}

class BlockARBase(mat: Material, mapClr: MapColor, final val name: String) extends Block(mat, mapClr) {

    private val itemBlock = new ItemBlockARBase(this)

    this.setUnlocalizedName(name)

    def getName: String = name

    def getItemBlock: ItemBlockARBase = itemBlock

}
