package diddi.advancedrefineries.block.base

import net.minecraft.item.ItemBlock

class ItemBlockARBase(block: BlockARBase) extends ItemBlock(block) {

    this.setHasSubtypes(false)
    this.setMaxDamage(0)

    override def getMetadata(damage: Int): Int = damage
}
