package diddi.advancedrefineries.block

import net.minecraft.block.material.{MapColor, Material}
import net.minecraft.block.properties.PropertyBool
import net.minecraft.block.state.{BlockFaceShape, BlockStateContainer, IBlockState}
import net.minecraft.util.EnumFacing
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import diddi.advancedrefineries.block.base.BlockARBase
import net.minecraft.block.Block

object BlockStrut {

    final val CONN_UP: PropertyBool = PropertyBool.create("up")
    final val CONN_DOWN: PropertyBool = PropertyBool.create("down")
    final val CONN_NORTH: PropertyBool = PropertyBool.create("north")
    final val CONN_SOUTH: PropertyBool = PropertyBool.create("south")
    final val CONN_WEST: PropertyBool = PropertyBool.create("west")
    final val CONN_EAST: PropertyBool = PropertyBool.create("east")

}

class BlockStrut(mat: Material, color: MapColor, name: String, canConnectLambda: (IBlockAccess, BlockPos, EnumFacing) => Boolean) extends BlockARBase(mat, color, name) {

    def this(mat: Material, color: MapColor, name: String) = this(mat, color, name, null)

    override def getActualState(state: IBlockState, w: IBlockAccess, p: BlockPos): IBlockState = {

        state.withProperty(BlockStrut.CONN_UP, canConnect(w, p, EnumFacing.UP).asInstanceOf[java.lang.Boolean]).
                withProperty(BlockStrut.CONN_DOWN, canConnect(w, p, EnumFacing.DOWN).asInstanceOf[java.lang.Boolean]).
                withProperty(BlockStrut.CONN_NORTH, canConnect(w, p, EnumFacing.NORTH).asInstanceOf[java.lang.Boolean]).
                withProperty(BlockStrut.CONN_SOUTH, canConnect(w, p, EnumFacing.SOUTH).asInstanceOf[java.lang.Boolean]).
                withProperty(BlockStrut.CONN_WEST, canConnect(w, p, EnumFacing.WEST).asInstanceOf[java.lang.Boolean]).
                withProperty(BlockStrut.CONN_EAST, canConnect(w, p, EnumFacing.EAST).asInstanceOf[java.lang.Boolean])
    }

    private def canConnect(w: IBlockAccess, p: BlockPos, dir: EnumFacing): Boolean = {
        var lambda = false
        if (canConnectLambda != null)
            lambda = canConnectLambda(w, p, dir)

        w.getBlockState(p.offset(dir)).getBlock == this || w.getBlockState(p.offset(dir)).getBlockFaceShape(w, p.offset(dir), dir.getOpposite) == BlockFaceShape.SOLID || lambda
    }

    override def isOpaqueCube(state: IBlockState): Boolean = false

    override def isFullBlock(state: IBlockState): Boolean = false

    override def getMetaFromState(state: IBlockState): Int = 0

    override def isPassable(worldIn: IBlockAccess, pos: BlockPos): Boolean = false

    // todo: IBlockState.getBlockFaceShape is the replacement here, but where to put that?
    // override def isBlockSolid(worldIn: IBlockAccess, pos: BlockPos, side: EnumFacing): Boolean = false

    override def canPlaceTorchOnTop(state: IBlockState, world: IBlockAccess, pos: BlockPos): Boolean = false

    override def createBlockState(): BlockStateContainer = new BlockStateContainer(this,
        BlockStrut.CONN_UP,
        BlockStrut.CONN_DOWN,
        BlockStrut.CONN_NORTH,
        BlockStrut.CONN_SOUTH,
        BlockStrut.CONN_WEST,
        BlockStrut.CONN_EAST)

}
